package fr.afpa.main;


import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Random;

import java.util.Scanner;

import javax.imageio.ImageIO;
import javax.swing.JFrame;

import fr.afpa.beans.Room;
import fr.afpa.views.Menu;
import fr.afpa.beans.Character;
import fr.afpa.beans.Generator;
import fr.afpa.beans.Item;
import fr.afpa.beans.Monster;


public class Game extends JFrame{
	static Menu menu = new Menu();
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Window window;
	
	private BufferedImage img;
	
	public Game() {
		
		importImg();
		
		setSize(640,640);
		setVisible(true);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setLocationRelativeTo(null);
		window = new Window(img);
		add(window);
	}
	

	private void importImg() {
		
		InputStream is = getClass().getResourceAsStream("/res/spriteLaby.png");
		
		try {
			img = ImageIO.read(is);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}


	public static void main(String[] args) {
		
		Scanner in = new Scanner(System.in);
		
		System.out.print("Quelle taille de donjon souhaitez-vous ? Largeur:");
		int sizeX = in.nextInt();
		System.out.print("  Longueur:");
		int sizeY = in.nextInt();
		Generator generator = new Generator(sizeX,sizeY);
		afficherTab(generator.getTab());
		game(generator.getTab());

	}
	
	// Variables static :
	
	static int etage = 1;
	static Character hero = new Character(0,0);

	public static void afficherTab(Room[][] laby) {	
		System.out.println("\n" + "\n" + "\n" + "\n" + "\n" + "\n" + "\n" + "\n" + "\n" + "\n" + "\n" + "\n" + "\n"+ "\n"+ "\n"+ "\n"+ "\n"+ "\n"+ "\n"+ "\n"+ "\n");
		System.out.println("Etage numéro : " + etage);
		for (int y = 0; y < laby[0].length; y++) {
			for (int x = 0; x < laby.length; x++) {
				System.out.print((laby[x][y].iswUp()) ? "+---" : "+   ");
				
				
			}
			System.out.print("+");
			System.out.println();
			String result = "";
			for (int x = 0; x < laby.length; x++) {
				if (laby[x][y].isPlayer()) {
					result = "P";
				} else if (laby[x][y].isExit()) {
					result = "▒";
				}else if (laby[x][y].getItemsList().size() != 0) {
					result = "o";
					if(laby[x][y].getItemsList().size() != 1) {
						result =  laby[x][y].getItemsList().size()+result;
					}
				}else if (laby[x][y].getMonstersList().size() != 0) {
					result = "M";
					if(laby[x][y].getMonstersList().size() != 1) {
						result =  laby[x][y].getMonstersList().size()+result;
					}
				}else {
					result = " ";
				}
				if(result.length() != 1) {
					System.out.print((laby[x][y].iswLeft()) ? "|"+result+" " : " "+result+" ");
				}else {
					System.out.print((laby[x][y].iswLeft()) ? "| "+result+" " : "  "+result+" ");
				}
			}
			System.out.print("|");
			System.out.print("\n");
		}
		
		for (int x = 0; x < laby.length; x++) {
			System.out.print((laby[x][laby[x].length-1].iswDown()) ? "+---" : "+   ");
		}
		System.out.print("+");	
		System.out.println("");
	}

	public static void game(Room[][] laby) {
		String msg = "";
		String choice = "";
		Item item = new Item();
		Scanner in = new Scanner(System.in);
		Generator generator = new Generator();
		int y = 0;
		int x = 0;
		
		for(int i  = 0; i < laby.length; i++) {
			for(int j = 0; j< laby[i].length; j++) {
				if(laby[i][j].isPlayer()) {
					x= i;
					y= j;
					i= laby.length;
					break;
				}
			}
		}

		while (!choice.equalsIgnoreCase("P")) {
			afficherTab(laby);
			if(!(msg.equals(""))) System.out.println(msg);
			msg= "";
			if(laby[x][y].isPlayer() && laby[x][y].getMonstersList().size() != 0) {
				menu.fightMenu(laby[x][y]);
				int targetChoice = in.nextInt();
				Monster monster = new Monster();
				if(targetChoice > laby[x][y].getMonstersList().size() || targetChoice < 0) {
					msg = "Vous trébuchez comme du n'importe quoi";
				}else {
					monster = laby[x][y].getMonster(targetChoice-1);
					hero.fight(monster);
					if(monster.getLife() <= 0) {
						switch(monster.getName()) {
						case "Vampire":
							msg = "Vous tabassez une pauvre chauve-souris !\n"+
									"HP restant :" + hero.getLife()+ " HP";
							break;
						case "Gobelin":
							msg = "Vous écrasez ce pauvre gobelin !\n"+
									"HP restant : " + hero.getLife()+ " HP";
							break;
						}
						laby[x][y].getMonstersList().remove(monster);
					}
				}
			}else if(laby[x][y].isPlayer() && laby[x][y].getItemsList().size() != 0) {
				menu.itemsMenu(laby[x][y]);	
				int itemChoice = in.nextInt();
				if(itemChoice > laby[x][y].getItemsList().size() || itemChoice < 1) {
					msg = "Des problèmes de vue ? Il n'y a pas d'objet ici !";
				}else {					
					item = laby[x][y].getItem(itemChoice-1);
					
					hero.useItem(item);
					
					switch(item.getName()) {
					case "Potion de force":
						msg = "Vous vous sentez plus fort !\n"+
								"+ " + item.getValueEffect()+ " STR";
						break;
					case "Potion de vie":
						msg = "Vous vous sentez revivre\n"+
								"+ " + item.getValueEffect()+ " HP";
						break;
					case "Sac d'or":
						msg = "Vous vous en foutez plein les poches !\n"+
								"+ " + item.getValueEffect()+ " GOLD";
						break;
					}
					laby[x][y].removeItems();
				}
				
			} else if(laby[x][y].isExit() && laby[x][y].isPlayer() ) {
				System.out.print("\n" +"Bravo vous avez réussi cette salle");
				System.out.print("\n" +"Pressez N pour passer au niveau suivant");
				String next = "";
				
				next = in.next();
				
				if(next.equalsIgnoreCase("N") ) {
					etage++;
					generator.generateDonjon(9,6);
					afficherTab(generator.getTab());
					game(generator.getTab());
				}
				
			}else {
				menu.directionMenu();
				choice = in.next();
				
				msg = "Vous êtes bloqué par un obstacle";
				switch (choice.toUpperCase()) {
				case "Z":
					if (!laby[x][y].iswUp()) {
						laby[x][y].setPlayer(false);
						laby[x][y - 1].setPlayer(true);
						y--;
						afficherTab(laby);
						hero.setPositionY(y);
						msg = "";
					}
					break;
					
				case "Q":
					if (!laby[x][y].iswLeft()) {
						laby[x][y].setPlayer(false);
						laby[x - 1][y].setPlayer(true);
						x--;
						afficherTab(laby);
						hero.setPositionX(x);
						msg = "";
					} 
					break;
				case "S":
					if (!laby[x][y].iswDown()) {
						laby[x][y].setPlayer(false);
						laby[x][y + 1].setPlayer(true);
						y++;
						afficherTab(laby);
						hero.setPositionY(y);
						msg = "";
					} 
					break;
				case "D":
					if (!laby[x][y].iswRight()) {
						laby[x][y].setPlayer(false);
						laby[x + 1][y].setPlayer(true);
						x++;
						afficherTab(laby);
						hero.setPositionX(x);
						msg = "";
					} 
					break;
				default :
					msg = "Mauvais input";
					break;
					
				}
			}
		}

		in.close();
	}



}
