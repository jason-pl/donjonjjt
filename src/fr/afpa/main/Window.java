package fr.afpa.main;

import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.util.ArrayList;

import javax.swing.JPanel;

public class Window extends JPanel {

	private static final long serialVersionUID = 1L;

	private BufferedImage img;

	private ArrayList<BufferedImage> sprites = new ArrayList<>();
	

	public Window(BufferedImage img) {
		this.img = img;

		loadSprites();
	}

	private void loadSprites() {
		for(int y=0; y < 10; y++) {
			for(int x = 0; x < 10; x++) {
				sprites.add(img.getSubimage(x *32, y *32, 32, 32));
			}
		}
	}

	public void paintComponent(Graphics g) {
		super.paintComponent(g);

		g.drawImage(sprites.get(0), 0, 0, null);
//		g.drawImage(img.getSubimage(0, 0, 32, 32),0, 0, null);

	}

}
