package fr.afpa.beans;


public class Character extends Entity{
	static int life;
	static int power;
	static int gold;
		
	public int getLife() {
		return life;
	}


	public void setLife(int life) {
		Character.life = life;
	}


	public int getPower() {
		return power;
	}


	public void setPower(int power) {
		Character.power = power;
	}


	public int getGold() {
		return gold;
	}


	public void setGold(int gold) {
		Character.gold = gold;
	}


	public Character() {
		super();
	}


	public Character(int x, int y) {
		super(x, y);
		this.setLife(100);
		this.setPower(10);
		// TODO Auto-generated constructor stub
	}
	
	
	// fonction actions : 
	
	public void fight (Monster monstre) {
		
		if (monstre.getLife() - this.getPower() <=0) {
			
			monstre.setLife(monstre.getLife()-this.getPower());
		}
		else {
			
			monstre.setLife(monstre.getLife()-this.getPower());
			this.setLife(this.getLife()-monstre.getPower());
		}
	}
	
	public void useItem(Item item) {
		
		switch (item.getName()) {
		case "Potion de Force" :
			this.setPower(getPower() + item.getValueEffect());
			break;
		case "Sac d'or" :
			this.setGold(this.getGold() + item.getValueEffect());
			break;
		case "Potion de vie":
			this.setLife(getLife() + item.getValueEffect());
			break;
		}
	}	
}
