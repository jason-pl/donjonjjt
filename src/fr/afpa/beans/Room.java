package fr.afpa.beans;

import java.util.ArrayList;

public class Room {
	
	
	// Attributs :
	
	int id;
	static int count;
	int counter;
	boolean wUp = true;
	boolean wDown = true;
	boolean wRight = true;
	boolean wLeft = true;
	boolean isVisited = false;
	boolean exit = false;
	boolean isPlayer = false;
	int x = 0;
	int y = 0;
	
	
	// tableau des Monstres / items de la salle:	
	
	ArrayList<Monster> monsterList = new ArrayList<Monster>();
	ArrayList <Item> items = new ArrayList<Item>();
	
	
	// Constructeurs : 
	
	public Room() {
		
	}

	public ArrayList<Monster> getMonstersList() {
		return monsterList;
	}
	public Monster getMonster(int i) {
		return monsterList.get(i);
	}


	public void setMonsterList(Monster monster) {
		this.monsterList.add(monster);
	}	

	public Room(int x, int y) {
		this.setX(x);
		this.setY(y);
		this.setId(count);
		count ++;
	}
	
	public Room(int x, int y, boolean wUp, boolean wDown,boolean wRight, boolean wLeft, boolean isPlayer) {
		count++;
		this.id = count;
		this.x = x;
		this.y = y;
		this.wUp = wUp;
		this.wRight = wRight;
		this.wDown = wDown;
		this.wLeft = wLeft;
		this.isPlayer = isPlayer;
	}
	
	
	// Fonctions utilitaires :
	
	public String toString() {
		return "Room [id=" + id + ", wUp=" + wUp + ", wDown=" + wDown + ", wRight=" + wRight + ", wLeft=" + wLeft
				+ ", isVisited=" + isVisited + ", count :" + getCounter() + "]";
	}
	
	public String affichage() {
		return "" + 0;
	}
	
	public int countWall() {
		count = 0;
		if (this.iswUp()){
			count++;
		}
		if (this.iswRight()) {
			count++;
		}
		if (this.iswDown()) {
			count++;
		}
		if (this.iswLeft()) {
			count++;
		}
		return count;
	}
	
	
	// Getters / Setters (ArrayList) : 
	
	public ArrayList<Monster> getMonsterList() {
		return monsterList;
	}

	public void setMonsterList(ArrayList<Monster> liste) {
		this.monsterList = liste;
	}

	public ArrayList <Item> getItemsList() {
		return items;
	}
	
	public void setItemsList(ArrayList<Item> items) {
		this.items = items;
	}
	
	
	// Getters / Setters (Attributs) : 
	
	public int getCounter() {
		return counter;
	}

	public void setCounter(int counter) {
		this.counter = counter;
	}
	
	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	public boolean isVisited() {
		return isVisited;
	}

	public void setVisited(boolean isVisited) {
		this.isVisited = isVisited;
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public boolean isPlayer() {
		return isPlayer;
	}

	public void setPlayer(boolean isPlayer) {
		this.isPlayer = isPlayer;
	}
 	
	public boolean iswUp() {
		return wUp;
	}
	
	public void setwUp(boolean wUp) {
		this.wUp = wUp;
	}
	
	public boolean iswDown() {
		return wDown;
	}
	
	public void setwDown(boolean wDown) {
		this.wDown = wDown;
	}
	
	public boolean iswRight() {
		return wRight;
	}
	
	public void setwRight(boolean wRight) {
		this.wRight = wRight;
	}
	
	public boolean iswLeft() {
		return wLeft;
	}
	
	public void setwLeft(boolean wLeft) {
		this.wLeft = wLeft;
	}
	
	public Item getItem(int i) {
		return items.get(i);
	}

	public boolean isExit() {
		return exit;
	}
	
	public void removeItems() {
		this.items.removeAll(items);
	}

	public void setItems(Item items) {
		this.items.add(items);
	}
	public void setExit(boolean exit) {
		this.exit = exit;
	}
}

