package fr.afpa.beans;

public class StrengthPotion extends Item {
	
	public StrengthPotion(int effect) {
		super(effect);
		setName("Potion de force");
	}

}
