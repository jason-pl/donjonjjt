package fr.afpa.beans;

public class Item {
	int id;
	String name;
	int effect;
	
	public Item() {
	}
	
	public Item(int effect) {
		this.effect = effect;
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getValueEffect() {
		return effect;
	}

	public void setValueEffect(int effect) {
		this.effect = effect;
	}


}
