package fr.afpa.beans;

import java.util.Random;

public abstract class Entity {
	// Attributs : 
	
	private int idEntity;
	protected int positionX;
	protected int positionY;
	protected int life;
	protected int power;
	protected int gold;
	
	
	// Constucteurs : 
	
	public Entity(int x, int y) {
		this.positionX= x;
		this.positionY= y;
		this.gold = this.generationGold();

	}
	
	public Entity() {
		
	}
	
	
	// Generation gold :
	
	public int generationGold() {
	Random randObject = new Random();
	this.gold = randObject.nextInt(20 - 5 + 1) + 5; // de 5 � 20 golds
	return gold;		
	}
	

	// Getters / Setters : 
	
	public int getPositionX() {
		return positionX;
	}

	public void setPositionX(int positionX) {
		this.positionX = positionX;
	}

	public int getPositionY() {
		return positionY;
	}

	public void setPositionY(int positionY) {
		this.positionY = positionY;
	}

	public int getIdEntity() {
		return idEntity;
	}
	
}