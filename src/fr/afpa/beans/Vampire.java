package fr.afpa.beans;

import java.util.Random;

public class Vampire extends Monster {

	
	// Attributs : 
	
	private int idVampire;

	public static int totalVampire = 1;

	
	// Constructeurs : 
	
	public Vampire(int x, int y) {
		super(x, y);
		this.setName("Vampire");
		this.life = generationLifeVampire();
		this.power = generationPowerVampire();
		this.idVampire = totalVampire;
		totalVampire++;
	}
	
	
	// Generation power/life
	
	public int generationLifeVampire() {
		Random randObject = new Random();
		this.life = randObject.nextInt(40 - 20 + 1) + 20; // 20 à 40 pdv
		return life;		
		}

	public int generationPowerVampire() {
		Random randObject = new Random();
		this.gold = randObject.nextInt(5 - 3 + 1) + 3; // 3 à 5 attq
		return gold;		
		}
	
	
	// Getters / Setters :
	
	public int getIdVampire() {
		return idVampire;
	}			
}
