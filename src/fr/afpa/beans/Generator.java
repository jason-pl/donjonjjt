package fr.afpa.beans;

import java.util.ArrayList;
import java.util.Random;

public class Generator {
	static Room [][] tab;
	
	public Generator() {
	}
	
	public Generator(int x, int y) {
		generateDonjon(x,y);
	}

	public Room[][] getTab() {
		return tab;
	}

	public void setTab(Room[][] tab) {
		Generator.tab = tab;
	}

	public void generateDonjon(int x, int y) {
		boolean isOver = false;
		boolean allVisited = false;
		String result = "";
		Random random = new Random();
		
		ArrayList<Room> history = new ArrayList<Room>();
		ArrayList<String> direction = new ArrayList<String>(); //tableau des directions
		
		// génère la taille du donjon
		Room tab[][] = new Room[x][y];
		for (int xx = 0; xx < x; xx++) {
			for (int yy = 0; yy < y; yy++) {
				tab[xx][yy] = new Room(xx, yy);
			}
		}
		
		// Permet de démarrer à un indice x et y aléatoire lors de la génération du donjon
		int posX = random.nextInt(x);
		int posY = random.nextInt(y);

		// enregistre les différentes positions par lesquels nous passon dans le cas où
		// plus aucune direction n'est possible
		
		
		//enregistre la première salle
		history.add(tab[posX][posY]);
		tab[posX][posY].setVisited(true);
		
		while(!allVisited) {
			isOver = false;
			while (!isOver) {
				isOver = true;
				direction.removeAll(direction);
				if (tab[posX][posY].getX() != 0 && !(tab[posX-1][posY].isVisited()))
					direction.add("left");
				if (tab[posX][posY].getX() != x - 1 && !(tab[posX+1][posY].isVisited()))
					direction.add("right");
				if (tab[posX][posY].getY() != 0 && !(tab[posX][posY-1].isVisited()))
					direction.add("up");
				if (tab[posX][posY].getY() != y - 1 && !(tab[posX][posY+1].isVisited()))
					direction.add("down");
				
				if(direction.size() == 0) {
					if(history.size() == 0) {
						allVisited = true;
						break;
					}
					posX = history.get(history.size() - 1).getX();
					posY = history.get(history.size() - 1).getY();
					history.remove(history.size() - 1);
					isOver = false;
				}
			}
			if(history.size() != 0) {
				result = direction.get(random.nextInt(direction.size()));
				switch (result) {
				case "right":
					tab[posX][posY].setwRight(false);
					posX++;
					tab[posX][posY].setwLeft(false);
					break;
				case "left":
					tab[posX][posY].setwLeft(false);
					posX--;
					tab[posX][posY].setwRight(false);
					break;
				case "down":
					tab[posX][posY].setwDown(false);
					posY++;
					tab[posX][posY].setwUp(false);
					break;
				case "up":
					tab[posX][posY].setwUp(false);
					posY--;
					tab[posX][posY].setwDown(false);
					break;
				}
				direction.remove(result);
				
				history.add(tab[posX][posY]);
				tab[posX][posY].setVisited(true);
			}
		}

		this.generateStartExit(tab);
		this.generateItems(tab);
		this.generateMonster(tab);

		this.setTab(tab);
	}
	
	public void generateStartExit(Room[][] tab) {
		ArrayList<Room> entryPoint = new ArrayList<Room>();
		ArrayList<Room> intersectPoint = new ArrayList<Room>();
		ArrayList<String> direction = new ArrayList<String>();
		ArrayList<Room> bestPath = new ArrayList<Room>();
		
		int max = 0;
		
		Random random = new Random();
		
		for(int xx = 0; xx < tab.length; xx++) {
			for(int yy = 0; yy < tab[xx].length; yy++) {
				
				//comptage du nombre d'occurence de point de d�part
				if(tab[xx][yy].countWall() == 3) {
					entryPoint.add(tab[xx][yy]); 
				}
			}
		}
		int entryCount = 0;
		
		boolean over = false;
		while(!over) {
			int count = 0;
			int x = 0;
			int y = 0;
			if(intersectPoint.size() == 0 && entryPoint.size() == entryCount) {
				break;
			}
			if(intersectPoint.size() == 0) {
				resetRoomVisited(tab);
				resetRoomCounter(tab);
				x = entryPoint.get(entryCount).getX();
				y = entryPoint.get(entryCount).getY();
				count = 0;
				entryCount ++;
			} else {
				x = intersectPoint.get(0).getX();
				y = intersectPoint.get(0).getY();
				count = tab[x][y].getCounter();
				intersectPoint.remove(0);
			}
			
			boolean pathIsOver = false;
			while(!pathIsOver) {
				tab[x][y].setVisited(true);
				direction.removeAll(direction);
				if(!(tab[x][y].iswUp())&& !(tab[x][y-1].isVisited())) direction.add("up");
				if(!(tab[x][y].iswDown())&& !(tab[x][y+1].isVisited()))direction.add("down");
				if(!(tab[x][y].iswRight())&& !(tab[x+1][y].isVisited()))direction.add("right");
				if(!(tab[x][y].iswLeft())&& !(tab[x-1][y].isVisited()))direction.add("left");
				

				switch(direction.size()) {
				case 0 :
					tab[x][y].setCounter(count);
					if(count > max) {
						max = count;
						if(bestPath.size() != 0) {							
							bestPath.removeAll(bestPath);

						}
						bestPath.add(tab[entryPoint.get(entryCount-1).getX()][entryPoint.get(entryCount-1).getY()]);
						bestPath.add(tab[x][y]);
					}
					pathIsOver = true;
					break;
				case 2 :
					if(!(intersectPoint.contains(tab[x][y]))) {						
						tab[x][y].setCounter(count);
						intersectPoint.add(tab[x][y]);
					}
					break;
					
				}
				if(direction.size() == 0) {
					break;
				} else {				
					String result = direction.get(random.nextInt(direction.size()));
					switch(result) {
					case "up":
						y--;
						break;
					case "down":
						y++;
						break;
					case "right":
						x++;
						break;
					case "left":
						x--;
						break;
					}
					count++;
				}
			}
		}
		bestPath.get(0).setPlayer(true);
		bestPath.get(1).setExit(true);
	}
	
	public void generateMonster(Room[][]tab){
		Random random = new Random();
		for(Room [] roomTab : tab) {
			for(Room room : roomTab) {
				if(room.countWall() == 1 &&  random.nextInt(100) > 50) {
					int randomNumber = random.nextInt(3)+1;					
					for(int i = 0; i < randomNumber; i ++) {
						room.setMonsterList(generateRandomMonster(room.getX(),room.getY()));
					}
				}
			}
		}
	}
	
	public Monster generateRandomMonster(int x, int y) {
		
		Monster monster = new Monster(x,y);
		Random randObject = new Random();
		int nbRandom = randObject.nextInt(2);
		if (nbRandom == 0) {
			monster = new Vampire(x,y);
		}
		else {
			monster = new Gobelin(x,y);
			
		}
		return monster;
	}
	
	public void generateItems(Room [][] tab) {
		Random random = new Random();
		
		for(Room [] roomTab : tab) {
			for(Room room : roomTab) {
				
				ArrayList <Item> itemsRoom = new ArrayList<Item>();
				
				if(room.countWall() == 3 && !(room.isPlayer()) && !(room.isExit()) && random.nextInt(100) > 50) {
					
					int randomNumber = random.nextInt(3)+1;
					ArrayList <String> itemPossibility = new ArrayList<String>();
					
					itemPossibility.add("life");
					itemPossibility.add("strength");
					itemPossibility.add("gold");
					
					for(int i = 0; i < randomNumber; i ++) {
						String result = itemPossibility.get(random.nextInt(itemPossibility.size()));
						switch(result) {
						case "life":
							room.setItems(new LifePotion(random.nextInt(10)+10));							
							break;
						case "strength":
							room.setItems(new StrengthPotion(random.nextInt(4)+1));
							break;
						case "gold":
							room.setItems(new GoldBag(random.nextInt(40)+10));
							break;
							
						}
						
						itemPossibility.remove(result);
					}
				}
			}
		}
	}
	
	public void resetRoomVisited(Room [][] tab) {
		for(int i = 0; i<tab.length; i++) {
			for(int j = 0 ; j <tab[i].length; j++) {
				tab[i][j].setVisited(false);
			}
		}
	}
	
	public void resetRoomCounter(Room [][] tab) {
		for(int i = 0; i<tab.length; i++) {
			for(int j = 0 ; j <tab[i].length; j++) {
				tab[i][j].setCounter(0);
			}
		}
	}
}
