package fr.afpa.beans;

import java.util.Random;

public class Gobelin extends Monster {

	
	// Attributs : 
	
	private int idGobelin;

	public static int totalGobelin = 1;
	
	
	// Constructeurs : 
	
	public Gobelin(int x, int y) {
		super(x, y);
		this.setName("Gobelin");
		this.life = generationLifeGobelin();
		this.power = generationPowerGobelin();
		this.idGobelin = totalGobelin;
		totalGobelin++;	
	}
	
	
	// Generation power/life
	
	public int generationLifeGobelin() {
		Random randObject = new Random();
		this.life = randObject.nextInt(20 - 10 + 1) + 10; // 10 à 20 pdv
		return life;		
		}

	public int generationPowerGobelin() {
		Random randObject = new Random();
		this.gold = randObject.nextInt(10 - 5 + 1) + 5; // 5 à 10 attq
		return gold;		
		}
	
	
	// Getters / Setters :
	
	public int getIdGobelin() {
		return idGobelin;
	}
}
