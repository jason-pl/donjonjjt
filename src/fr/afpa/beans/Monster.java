package fr.afpa.beans;

public class Monster extends Entity {
	
	
	// Attributs :
	protected String name;
	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}

	protected int idMonster;
	protected int life;
	protected int power;

	public static int totalMonster = 1;
	
	
	// Constructeurs :
	
	public Monster(int x, int y) {
		super(x,y);
		this.idMonster = totalMonster;
		
		totalMonster++;
	}

	
	// Generation gold :
	
		public Monster() {
		// TODO Auto-generated constructor stub
	}

	// Getters / Setters :
	
	public int getIdMonster() {
		return idMonster;
	}

	public int getLife() {
		return life;
	}

	public void setLife(int life) {
		this.life = life;
	}

	public int getPower() {
		return power;
	}

	public void setPower(int power) {
		this.power = power;

	}	

	public int getGold() {
		return gold;
	}

	public void setGold(int gold) {
		this.gold = gold;
	}
}
