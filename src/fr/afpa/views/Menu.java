package fr.afpa.views;

import fr.afpa.beans.Character;
import fr.afpa.beans.Room;

public class Menu {
	Character hero = new Character();

	public Menu() {
	}
	
	public void mainMenu() {
		
	}
	
	public void directionMenu() {
		String life = "";
		for(int j = 0;j < 10; j++) {
			if(j < hero.getLife()/10) {
				
				life += "❤";
			} else {
				life += " ";
			}
		}
		String directionMenu = 
			   " _____________________________________\n"+
			   "|              DIRECTION              |\n"+
			   "|-------------------------------------|\n"+
			   "|                                     |\n"+
			   "|          ["+life+         "]        |\n"+
			   "|                                     |\n"+
			   "|                Haut                 |\n"+
			   "|                                     |\n"+
			   "|                 Z                   |\n"+
			   "|     Gauche    Q   D     Droite      |\n"+
			   "|                 S                   |\n"+
			   "|                                     |\n"+
			   "|                Bas                  |\n"+
			   "|_____________________________________|\n"+
			   " Input: ";
		
		System.out.print(directionMenu);

	}
	
	public void itemsMenu(Room room) {
		String items = "";
		String result = "";
		String life = "";
		for(int j = 0;j < 10; j++) {
			if(j < hero.getLife()/10) {
				
				life += "❤";
			} else {
				life += " ";
			}
		}
		for(int i = 0; i < room.getItemsList().size(); i++) {
			switch(room.getItemsList().get(i).getName()) {
				case "Sac d'or":
					result = "Sac d'or       ";
					break;
				case "Potion de force":
					result = "Potion de force";
					break;
				case "Potion de vie":
					result = "Potion de vie  ";
					break;
			}
			items += "|          "+(i+1)+" - "+result+"        |\n";
		}
		
		String itemsMenu = 
				" _____________________________________\n"+
			    "|              ITEM MENU              |\n"+
			    "|-------------------------------------|\n"+
			    "|                                     |\n"+
			    "|          ["+life+         "]        |\n"+
			    "|                                     |\n"+
			    	items+
		    	"|                                     |\n"+
			    "|_____________________________________|\n"+
			    " Input: ";
		System.out.println(itemsMenu);
		
	}
	
	public void fightMenu(Room room) {
		String monster = "";
		String result = "";
		
		for(int j = 0;j < 10; j++) {
			if(j < hero.getLife()/10) {
				
				result += "💖";
			} else {
				result += " ";
			}
		}
		for(int i = 0; i < room.getMonstersList().size(); i++) {
			monster += "|          "+(i+1)+" - "+room.getMonster(i).getName()+" ["+(room.getMonster(i).getLife() == 1 ? room.getMonster(i).getLife()+" " : room.getMonster(i).getLife())+"]           |\n";
		}
		
		String monstersMenu = 
				" _____________________________________\n"+
			    "|               FIGHT MENU            |\n"+
			    "|-------------------------------------|\n"+
			    "|                                     |\n"+
			    "|          ["+result+       "]        |\n"+
			    "|                                     |\n"+
			    	monster+
		    	"|                                     |\n"+
			    "|_____________________________________|\n"+
			    " Input: ";
		System.out.println(monstersMenu);
		
	}
	
}
